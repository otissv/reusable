import React, { useContext, Fragment } from 'react'
import { UserContext } from './UserContext'
import { useForm } from 'reusable'

export function UserEdit() {
  const [
    {
      selected: { id, name, username },
    },
    dispatchUsers,
  ] = useContext(UserContext)

  const initialState = {
    fields: [
      {
        attributes: {
          name: 'children',
        },
        component: props => {
          console.log(props)
          return <div>test</div>
        },
        children: [
          {
            attributes: {
              name: 'name',
              label: 'Name',
              value: name,
            },
          },
          {
            attributes: {
              name: 'username',
              label: 'Username',
              value: username,
            },
          },
        ],
      },
    ],
  }

  const { dispatch, model, handleSubmit, ...formRest } = useForm({
    initialState,
    onSubmit: handleSubmit,
    onChange: o => console.log(o),
  })

  function handleSubmit(values, model) {
    console.log(values, model)
  }
  console.log(model)
  return (
    <Fragment>
      <h1>Edit User</h1>

      <form onSubmit={handleSubmit}>
        <button onClick={handleSubmit}>Save</button>
      </form>
    </Fragment>
  )
}
