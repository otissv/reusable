import React from 'react'
import { useCollectionReducer, useReducerDevtools } from 'reusable'

const initialState = {
  collections: [
    { id: 0, name: 'Sereeta', username: 'user_0', selected: false },
    { id: 1, name: 'Dollen', username: 'user_1', selected: false },
    { id: 2, name: 'Isabol', username: 'user_2', selected: false },
  ],
}

export function TodoList() {
  const todoReducer = useCollectionReducer(reducer, initialState)
  const [todos, dispatchTodo] = useReducerDevtools(
    todoReducer,
    initialState,
    'Todos'
  )

  function reducer(state, action, { create, index, read, remove, update }) {
    switch (action.type) {
      case 'SET_SELECTED': {
        const data = { selected: action.selected }
        const query = item => item.id === action.id
        return {
          ...state,
          collections: update(state.collections)(query)(data),
        }
      }
      case 'ITEMS_TO_REMOVE':
        return { ...state, itemsToRemove: action.itemsToRemove }

      default:
        return state
    }
  }

  function handleChange(e) {
    e.preventDefault()
    dispatchTodo({
      type: 'SET_SELECTED',
      selected: e.target.checked,
      id: parseInt(e.target.id, 10),
    })
  }
  function handleAdd(e) {
    e.preventDefault()
  }

  function handleDelete(e) {
    e.preventDefault()
  }

  function handleEdit(e) {
    e.preventDefault()
  }

  function handleSave(e) {
    e.preventDefault()
  }

  return (
    <div>
      <h1>Example</h1>
      <div>
        <table>
          <thead>
            <tr>
              <th />
              <th>Name</th>
              <th>Username</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {todos.collections.map(
              TableRow({ handleChange, handleDelete, handleEdit })
            )}
          </tbody>
        </table>
      </div>
    </div>
  )
}

function TableRow({ handleChange, handleDelete, handleEdit }) {
  return ({ id, completed, name, username, selected }) => (
    <tr key={id}>
      <td>
        <label htmlFor={id}>
          <input
            id={id}
            name={id}
            type="checkbox"
            checked={selected}
            onChange={handleChange}
          />
        </label>
      </td>

      <td>{name} </td>
      <td>{username}</td>
      <td>
        {`${selected}`}
        <button onClick={handleDelete} data-todo={id}>
          Delete
        </button>
        <button onClick={handleEdit} data-todo={id}>
          Edit
        </button>
      </td>
    </tr>
  )
}
