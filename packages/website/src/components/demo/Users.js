import React, { useContext } from 'react'
import { UserContext } from './UserContext'
import { RouterContext } from 'reusable'

export function Users() {
  const [{ collections: users }, dispatchUsers] = useContext(UserContext)
  const routerContext = useContext(RouterContext)
  const setRoute = routerContext[1]

  const parseId = e =>
    e.target.dataset.id
      ? parseInt(e.target.dataset.id, 10)
      : parseInt(e.target.id, 10)

  function handleChange(e) {
    e.preventDefault()
    dispatchUsers({
      type: 'SET_CHECKED',
      checked: e.target.checked,
      id: parseId(e),
    })
  }

  function handleAdd(e) {
    e.preventDefault()
  }

  function handleDelete(e) {
    e.preventDefault()
    const id = parseId(e)

    dispatchUsers({
      type: 'REMOVE',
      query: item => item.id === id,
    })
  }
  function handleEdit(e) {
    e.preventDefault()
    const id = parseId(e)

    setRoute(`/demo/edit/${id}`)
    dispatchUsers({
      type: 'SELECT',
      query: user => user.id === id,
    })
  }

  function handleSave(e) {
    e.preventDefault()
  }

  return (
    <div>
      <h1>View Users</h1>
      <div>
        <table>
          <thead>
            <tr>
              <th />
              <th>Name</th>
              <th>Username</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {users.map(TableRow({ handleChange, handleDelete, handleEdit }))}
          </tbody>
        </table>
      </div>
    </div>
  )
}

function TableRow({ handleChange, handleDelete, handleEdit }) {
  return ({ id, completed, name, username, checked }) => (
    <tr key={id}>
      <td>
        <input
          id={id}
          name={id}
          type="checkbox"
          checked={checked}
          onChange={handleChange}
        />
      </td>

      <td>{name} </td>
      <td>{username}</td>
      <td>
        {`${checked}`}
        <button onClick={handleDelete} data-id={id}>
          Delete
        </button>
        <button onClick={handleEdit} data-id={id}>
          Edit
        </button>
      </td>
    </tr>
  )
}
