import React from 'react'

import { CollectionProvider } from 'reusable'

const initialState = {
  collections: [
    { id: 0, name: 'Sereeta', username: 'user_0', checked: false },
    { id: 1, name: 'Dollen', username: 'user_1', checked: false },
    { id: 2, name: 'Isabol', username: 'user_2', checked: false },
  ],
}

function reducer(state, action, { create, index, read, remove, update }) {
  switch (action.type) {
    case 'SET_CHECKED': {
      const data = { checked: action.checked }
      const query = item => item.id === action.id
      return {
        ...state,
        collections: update(state.collections)(query)(data),
      }
    }
    case 'ITEMS_TO_REMOVE':
      return { ...state, itemsToRemove: action.itemsToRemove }

    default:
      return state
  }
}

export const UserContext = React.createContext([])

export function UserProvider({ children }) {
  return (
    <CollectionProvider
      value={initialState}
      name="Users"
      context={UserContext}
      reducer={reducer}
    >
      {children}
    </CollectionProvider>
  )
}
