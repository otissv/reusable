import React from 'react'
import { Router, RouterProvider } from 'reusable'
import { UserProvider } from './UserContext'
import { Users } from './Users'
import { UserEdit } from './UserEdit'

export function Routes() {
  const routes = [
    { path: '/demo', component: Users },
    { path: '/demo/new', component: () => 'new' },
    { path: '/demo/edit/:id', component: UserEdit },
    { path: '/demo/:id', component: () => 'id' },
  ]
  return <Router routes={routes} />
}

export function App({ children }) {
  return (
    <RouterProvider>
      <UserProvider>
        <Routes />
      </UserProvider>
    </RouterProvider>
  )
}
