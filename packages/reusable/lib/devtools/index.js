"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _devtools = require("./devtools");

Object.keys(_devtools).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _devtools[key];
    }
  });
});