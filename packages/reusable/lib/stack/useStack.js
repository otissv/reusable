"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useStack = useStack;
exports.useFIFO = useFIFO;
exports.useLIFO = useLIFO;

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _react = require("react");

function useStack(stackType) {
  var initialState = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var stack = {
    FIFO: {
      add: function add(state, payload) {
        return [].concat((0, _toConsumableArray2.default)(state), [payload]);
      },
      remove: function remove(state, payload) {
        return state.length === 0 ? [] : state.slice(1, state.length);
      },
      take: function take(state) {
        return state[0];
      }
    },
    LIFO: {
      add: function add(state, payload) {
        return [].concat((0, _toConsumableArray2.default)(state), [payload]);
      },
      remove: function remove(state, payload) {
        return state.length === 0 ? [] : state.slice(0, state.length - 1);
      },
      take: function take(state) {
        console.log(state[state.length - 1]);
        return state[state.length - 1];
      }
    }
  }[stackType];

  function reducer(state, _ref) {
    var type = _ref.type,
        payload = _ref.payload;

    switch (type) {
      case 'ADD_TO_STACK':
        return stack.add(state, payload);

      case 'REMOVE_FROM_STACK':
        return stack.remove(state, payload);

      default:
        return state;
    }
  }

  var _useReducer = (0, _react.useReducer)(reducer, initialState),
      _useReducer2 = (0, _slicedToArray2.default)(_useReducer, 2),
      state = _useReducer2[0],
      dispatch = _useReducer2[1];

  var methods = {
    dispatch: dispatch,
    remove: function remove(payload) {
      return dispatch({
        type: 'REMOVE_FROM_STACK',
        payload: payload
      });
    },
    add: function add(payload) {
      return dispatch({
        type: 'ADD_TO_STACK',
        payload: payload
      });
    },
    take: function take(payload) {
      dispatch({
        type: 'REMOVE_FROM_STACK',
        payload: payload
      });
      return stack.take(state);
    }
  };
  return [state, methods];
}

function useFIFO() {
  var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  return useStack('FIFO', initialState);
}

function useLIFO() {
  var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  return useStack('LIFO', initialState);
}