"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _useStack = require("./useStack");

Object.keys(_useStack).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _useStack[key];
    }
  });
});