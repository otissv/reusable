"use strict";

var _utils = require("./utils");

test('Box - is working', function () {
  var actual = (0, _utils.Box)('user').map(function (str) {
    return str.toUpperCase();
  }).map(function (str) {
    return "".concat(str, "_1");
  }).fold(function (str) {
    return str;
  });
  expect(actual).toBe('USER_1');
});
test('stringToArray - Converts string to Array', function () {
  var actual = (0, _utils.stringToArray)('create read update delete');
  expect(actual).toEqual(['create', 'read', 'update', 'delete']);
});
test('stringToArray - returns Array if already an array', function () {
  var actual = (0, _utils.stringToArray)(['create', 'read', 'update', 'delete']);
  expect(actual).toEqual(['create', 'read', 'update', 'delete']);
});
test('functionCallback - value is not a function', function () {
  var actual = (0, _utils.functionCallback)('value')();
  expect(actual).toBe('value');
});
test('functionCallback - value  function', function () {
  var actual = (0, _utils.functionCallback)(function (value) {
    return value;
  })('value');
  expect(actual).toBe('value');
});
test('functionCallback - value is not a function and nullabe', function () {
  var actual = (0, _utils.functionCallback)('value', null)();
  expect(actual).toBe(null);
});