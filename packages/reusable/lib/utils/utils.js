"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Box = Box;
exports.stringToArray = stringToArray;
exports.functionCallback = functionCallback;

function Box(x) {
  return {
    map: function map(f) {
      return Box(f(x));
    },
    fold: function fold(f) {
      return f(x);
    },
    log: function log(f) {
      console.log(x);
      return Box(x);
    }
  };
}

function stringToArray(value) {
  return value != null ? typeof value === 'string' ? value.split(' ') : value : [];
}

function functionCallback(value, nullable) {
  var _arguments = arguments;
  return function (props) {
    return typeof value === 'function' ? value(props) : _arguments.length > 1 ? nullable : value;
  };
}