"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _notEqualEffect = require("./notEqualEffect");

Object.keys(_notEqualEffect).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _notEqualEffect[key];
    }
  });
});