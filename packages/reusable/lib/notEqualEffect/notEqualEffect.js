"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useNotEqualEffect = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _curry2 = _interopRequireDefault(require("ramda/src/curry"));

var _react = require("react");

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

var useNotEqualEffect = (0, _curry2.default)(function useNotEqualEffect(callback, args) {
  (0, _react.useEffect)(function () {
    return !_isEqual.default.apply(void 0, (0, _toConsumableArray2.default)(args)) && callback();
  });
});
exports.useNotEqualEffect = useNotEqualEffect;