"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.matchPermissions = matchPermissions;
exports.checkPermissions = checkPermissions;
exports.checkUserHasRoles = checkUserHasRoles;
exports.getRoleAccess = getRoleAccess;
exports.canUse = canUse;
exports.cannotUse = cannotUse;
exports.access = access;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _utils = require("../utils");

function matchPermissions(_ref) {
  var active = _ref.active,
      bool = _ref.bool,
      rules = _ref.rules;
  return function (previous, allow) {
    var isAllow = function () {
      return (0, _utils.functionCallback)(allow, false)({
        active: active,
        rules: rules
      });
    }();

    var filter = function filter(value) {
      return function (permission) {
        return bool ? permission === value : permission !== value;
      };
    };

    var result = isAllow ? isAllow === false ? [] : rules.filter(filter(isAllow)) : rules.filter(filter(allow));
    return [].concat((0, _toConsumableArray2.default)(previous), (0, _toConsumableArray2.default)(result));
  };
}

function checkPermissions(_ref2) {
  var active = _ref2.active,
      bool = _ref2.bool,
      _ref2$list = _ref2.list,
      list = _ref2$list === void 0 ? [] : _ref2$list,
      rules = _ref2.rules;
  return list.length > 0 ? list.reduce(matchPermissions({
    active: active,
    bool: bool,
    rules: rules
  }), []) : rules;
}

function checkUserHasRoles(_ref3) {
  var user = _ref3.user,
      currentRoles = _ref3.currentRoles;
  var userRoles = (0, _utils.stringToArray)(user.roles);
  var hasRoles = currentRoles.some(function (role) {
    return userRoles.includes(role);
  });
  return hasRoles ? userRoles : [];
}

function getRoleAccess() {
  var currentRoles = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var roles = (0, _utils.stringToArray)(currentRoles);
  return function (permissionsRoles) {
    return roles.reduce(function (previous, role) {
      return permissionsRoles[role] ? [].concat((0, _toConsumableArray2.default)(previous), (0, _toConsumableArray2.default)(permissionsRoles[role])) : previous;
    }, []);
  };
}

function canUse(_ref4) {
  var can = _ref4.can,
      active = _ref4.active,
      roles = _ref4.roles;
  var list = [].concat((0, _toConsumableArray2.default)((0, _utils.stringToArray)(can)), (0, _toConsumableArray2.default)(getRoleAccess(roles)(active.roles)));
  return function (rules) {
    return checkPermissions({
      active: active,
      bool: true,
      list: list,
      rules: rules
    });
  };
}

function cannotUse(_ref5) {
  var cannot = _ref5.cannot,
      active = _ref5.active;
  return function (rules) {
    return checkPermissions({
      active: active,
      bool: false,
      list: (0, _utils.stringToArray)(cannot),
      rules: rules
    });
  };
}

function access(_ref6) {
  var _ref6$can = _ref6.can,
      can = _ref6$can === void 0 ? [] : _ref6$can,
      _ref6$cannot = _ref6.cannot,
      cannot = _ref6$cannot === void 0 ? [] : _ref6$cannot,
      permissions = _ref6.permissions,
      has = _ref6.has,
      roles = _ref6.roles;

  var _permissions = (0, _slicedToArray2.default)(permissions, 1),
      active = _permissions[0];

  var currentRoles = (0, _utils.stringToArray)(has);
  return currentRoles.length > 0 ? checkUserHasRoles((0, _objectSpread2.default)({}, active, {
    currentRoles: currentRoles
  })) : (0, _utils.Box)(active.user.permissions).map(canUse({
    can: can,
    active: active,
    has: has,
    roles: roles
  })).map(cannotUse({
    cannot: cannot,
    active: active
  })).fold(function (result) {
    return result;
  });
}