"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.roles = exports.users = void 0;

var _access = require("./access");

var users = [{
  id: 1,
  name: 'Sam',
  roles: 'GUEST',
  permissions: ['update', 'read']
}, {
  id: 2,
  name: 'Otis',
  roles: 'USER'
}, {
  id: 3,
  name: 'Paul',
  roles: 'MANGER'
}, {
  id: 4,
  name: 'Alex',
  roles: 'ADMIN'
}, {
  id: 5,
  name: 'David',
  roles: 'ADMIN'
}];
exports.users = users;
var roles = {
  ADMIN: ['create', 'read', 'update', 'delete'],
  MANGER: ['create', 'read', 'update', 'delete'],
  USER: ['read', 'update'],
  GUEST: ['read']
};
exports.roles = roles;
test('checkUserHasRoles - return empty array if not role', function () {
  var user = {
    roles: 'GUEST',
    permissions: ['update', 'read']
  };
  var actual = (0, _access.checkUserHasRoles)({
    user: user,
    currentRoles: ['ADMIN']
  });
  expect(actual).toEqual([]);
});
test('checkUserHasRoles - returns users roles if match', function () {
  var user = {
    roles: 'GUEST',
    permissions: ['update', 'read']
  };
  var actual = (0, _access.checkUserHasRoles)({
    user: user,
    currentRoles: ['GUEST']
  });
  expect(actual).toEqual(['GUEST']);
});
test('getRoleAccess - returns empty array if role does not exist', function () {
  var user = {
    roles: 'GUEST',
    permissions: ['update', 'read']
  };
  expect((0, _access.getRoleAccess)(['WHO'])(roles)).toEqual([]);
});
test('getRoleAccess - returns role permissions if matched', function () {
  var user = {
    roles: 'GUEST',
    permissions: ['update', 'read']
  };
  expect((0, _access.getRoleAccess)(['ADMIN'])(roles)).toEqual(['create', 'read', 'update', 'delete']);
  expect((0, _access.getRoleAccess)(['GUEST'])(roles)).toEqual(['read']);
});
test('matchPermissions - return empty array if not allow is false', function () {
  var active = {
    user: {
      id: 1,
      roles: 'GUEST',
      permissions: ['update', 'read']
    }
  };
  var allow = false;
  var actual = (0, _access.matchPermissions)({
    active: active,
    bool: true,
    rules: ['update', 'read']
  })([], allow);
  expect(actual).toEqual([]);
});
test('matchPermissions - return empty array if not allow fn result is false', function () {
  var active = {
    user: {
      id: 1,
      roles: 'GUEST',
      permissions: ['update', 'read']
    }
  };

  var allow = function allow(_ref) {
    var active = _ref.active;
    return false;
  };

  var actual = (0, _access.matchPermissions)({
    active: active,
    bool: true,
    rules: ['update', 'read']
  })([], allow);
  expect(actual).toEqual([]);
});
test('matchPermissions - return role', function () {
  var active = {
    user: {
      id: 1,
      roles: 'GUEST',
      permissions: ['update', 'read']
    }
  };
  var allow = 'read';
  var actual = (0, _access.matchPermissions)({
    active: active,
    bool: true,
    rules: ['update', 'read']
  })([], allow);
  expect(actual).toEqual(['read']);
});
test('matchPermissions - allow fn return role', function () {
  var active = {
    user: {
      id: 1,
      roles: 'GUEST',
      permissions: ['update', 'read']
    }
  };

  var allow = function allow(_ref2) {
    var active = _ref2.active;
    return 'read';
  };

  var actual = (0, _access.matchPermissions)({
    active: active,
    bool: true,
    rules: ['update', 'read']
  })([], allow);
  expect(actual).toEqual(['read']);
});
test('matchPermissions - allow fn return role', function () {
  var active = {
    user: {
      id: 1,
      roles: 'GUEST',
      permissions: ['update', 'read']
    }
  };

  var allow = function allow(_ref3) {
    var active = _ref3.active;
    return 'delete';
  };

  var actual = (0, _access.matchPermissions)({
    active: active,
    bool: true,
    rules: ['update', 'read']
  })([], allow);
  expect(actual).toEqual([]);
});
test('matchPermissions - return empty array if not allow is false', function () {
  var active = {
    user: {
      id: 1,
      roles: 'GUEST',
      permissions: ['update', 'read']
    }
  };
  var allow = 'delete';
  var actual = (0, _access.matchPermissions)({
    active: active,
    bool: true,
    rules: ['update', 'read']
  })([], allow);
  expect(actual).toEqual([]);
});