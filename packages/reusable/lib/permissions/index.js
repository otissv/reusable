"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _access = require("./access");

Object.keys(_access).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _access[key];
    }
  });
});

var _Permissions = require("./Permissions");

Object.keys(_Permissions).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _Permissions[key];
    }
  });
});