"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PermissionProvider = PermissionProvider;
exports.usePermissions = usePermissions;
exports.Permission = Permission;
exports.PermissionContext = void 0;

var _react = _interopRequireWildcard(require("react"));

var _access = require("./access");

var PermissionContext = _react.default.createContext({});

exports.PermissionContext = PermissionContext;

function PermissionProvider(_ref) {
  var children = _ref.children,
      state = _ref.state,
      value = _ref.value;
  var permissions = (0, _react.useState)(value);
  var permissionsState = state || (0, _react.useState)(value);
  return _react.default.createElement(PermissionContext.Provider, {
    value: permissionsState
  }, children);
} // hook


function usePermissions(_ref2) {
  var can = _ref2.can,
      cannot = _ref2.cannot,
      has = _ref2.has,
      roles = _ref2.roles;
  var permissions = (0, _react.useContext)(PermissionContext);
  var activeAccess = (0, _access.access)({
    can: can,
    cannot: cannot,
    permissions: permissions,
    has: has,
    roles: roles
  });
  return activeAccess.length > 0;
} // if children, return component else boolean


function Permission(_ref3) {
  var can = _ref3.can,
      cannot = _ref3.cannot,
      children = _ref3.children,
      fallback = _ref3.fallback,
      has = _ref3.has,
      roles = _ref3.roles;
  var hasAccess = usePermissions({
    can: can,
    cannot: cannot,
    has: has,
    roles: roles
  });
  return hasAccess ? children ? children : hasAccess : fallback;
}