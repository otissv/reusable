"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useInput = useInput;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _react = require("react");

var _devtools = require("../devtools");

function useInput(initialValue) {
  var _ref;

  var _useState = (0, _react.useState)(initialValue),
      _useState2 = (0, _slicedToArray2.default)(_useState, 2),
      value = _useState2[0],
      setValue = _useState2[1];

  function handleChange(e) {
    setValue(e.target.value);
  }

  var type = typeof initialValue === 'boolean' ? 'checked' : 'value';
  return _ref = {}, (0, _defineProperty2.default)(_ref, type, value), (0, _defineProperty2.default)(_ref, "onChange", handleChange), _ref;
}