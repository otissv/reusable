"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _useInput = require("./useInput");

Object.keys(_useInput).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _useInput[key];
    }
  });
});