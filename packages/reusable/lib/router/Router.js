"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Redirect = Redirect;
exports.Router = exports.Screen = exports.Route = exports.Link = exports.RouterProvider = exports.RouterContext = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _react = _interopRequireWildcard(require("react"));

var _routeParser = _interopRequireDefault(require("route-parser"));

var _isEqualEffect = require("../isEqualEffect");

var RouterContext = _react.default.createContext('');

exports.RouterContext = RouterContext;

var RouterProvider = _react.default.memo(function RouterProvider(_ref) {
  var children = _ref.children,
      value = _ref.value,
      state = _ref.state;
  var routerState = (0, _react.useState)(value);

  var _ref2 = state || routerState,
      _ref3 = (0, _slicedToArray2.default)(_ref2, 2),
      route = _ref3[0],
      setRoute = _ref3[1];

  (0, _react.useEffect)(function () {
    window.addEventListener('popstate', function (e) {
      setRoute(e.currentTarget.location.pathname);
    });
  });
  (0, _react.useEffect)(function () {
    window.history.replaceState({}, route, route);
  }, [route]);
  return _react.default.createElement(RouterContext.Provider, {
    value: [route, setRoute]
  }, children);
});

exports.RouterProvider = RouterProvider;
RouterProvider.defaultProps = {
  value: window.location.pathname
};

var Link = _react.default.memo(function Link(_ref4) {
  var children = _ref4.children,
      onClick = _ref4.onClick,
      onEnter = _ref4.onEnter,
      to = _ref4.to,
      propsRest = (0, _objectWithoutProperties2.default)(_ref4, ["children", "onClick", "onEnter", "to"]);

  var _useContext = (0, _react.useContext)(RouterContext),
      _useContext2 = (0, _slicedToArray2.default)(_useContext, 2),
      route = _useContext2[0],
      setRoute = _useContext2[1];

  function handleClick(e) {
    e.preventDefault();

    if (to !== route) {
      setRoute(to);
      window.history.pushState({}, route, route);
    }

    onClick && onClick(e);
  }

  return _react.default.createElement("a", (0, _extends2.default)({
    href: to,
    onClick: handleClick
  }, propsRest), children);
});

exports.Link = Link;

function Redirect(_ref5) {
  var to = _ref5.to;
  var router = (0, _react.useContext)(RouterContext);
  var setRoute = router[1];
  (0, _react.useEffect)(function () {
    window.history.pushState({}, to, to);
    setRoute(to);
  }, [to]);
  return null;
}

var Route = _react.default.memo(function Route(_ref6) {
  var component = _ref6.component,
      onEnter = _ref6.onEnter,
      path = _ref6.path;

  var _useContext3 = (0, _react.useContext)(RouterContext),
      _useContext4 = (0, _slicedToArray2.default)(_useContext3, 1),
      route = _useContext4[0];

  var Component = component;
  var pattern = new _routeParser.default(path);
  var params = pattern.match(route);
  (0, _react.useEffect)(function () {
    if (params) {
      onEnter && onEnter();
    }
  }, [route]);
  return params ? _react.default.createElement(Component, {
    route: [(0, _objectSpread2.default)({}, route[0], {
      params: params
    }), route[1]]
  }) : null;
});

exports.Route = Route;

function flattenRoutes(routes, parent) {
  return routes.reduce(function (accumulator, route) {
    return route.children ? [].concat((0, _toConsumableArray2.default)(accumulator), (0, _toConsumableArray2.default)(flattenRoutes(route.children, route))) : [].concat((0, _toConsumableArray2.default)(accumulator), [(0, _objectSpread2.default)({}, route, {
      component: parent && function () {
        return parent.component.type ? parent.component.type({
          routes: parent.children
        }) : parent.component({
          routes: parent.children
        });
      } || route.component
    })]);
  }, []);
}

var Screen = _react.default.memo(function RouterComponent(_ref7) {
  var routes = _ref7.routes;

  var _useContext5 = (0, _react.useContext)(RouterContext),
      _useContext6 = (0, _slicedToArray2.default)(_useContext5, 1),
      route = _useContext6[0];

  var params = false;
  var Component = null;
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = routes[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var item = _step.value;
      var pattern = new _routeParser.default(item.path);
      var match = pattern.match(route);

      if (match) {
        params = match;
        Component = item.component;
        break;
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return params ? _react.default.createElement(Component, {
    route: [(0, _objectSpread2.default)({}, route[0], {
      params: params
    }), route[1]]
  }) : null;
});

exports.Screen = Screen;

var Router = _react.default.memo(function Router(props) {
  var routes = flattenRoutes(props.routes);
  return _react.default.createElement(Screen, {
    routes: routes
  });
});

exports.Router = Router;
Router.defaultProps = {
  routes: []
};