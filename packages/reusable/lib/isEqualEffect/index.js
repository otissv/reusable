"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _isEqualEffect = require("./isEqualEffect");

Object.keys(_isEqualEffect).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _isEqualEffect[key];
    }
  });
});