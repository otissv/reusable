"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useForm = useForm;
exports.buildModel = buildModel;
exports.validator = validator;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _curry2 = _interopRequireDefault(require("ramda/src/curry"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectSpread3 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _react = require("react");

var _cuid = _interopRequireDefault(require("cuid"));

var _isEqualEffect = require("../isEqualEffect");

//TODO: add hide boolean / function
function modelDefaults() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      id = _ref.id,
      isValid = _ref.isValid,
      parent = _ref.parent,
      touched = _ref.touched;

  return {
    dirty: touched || false,
    id: id || (0, _cuid.default)(),
    isValid: isValid || true,
    parent: parent && parent.id,
    touched: touched || false
  };
}

function useForm(_ref2) {
  var initialState = _ref2.initialState,
      onBlur = _ref2.onBlur,
      onChange = _ref2.onChange,
      onFocus = _ref2.onFocus,
      onSubmit = _ref2.onSubmit;
  var reducer = (0, _react.useCallback)(function reducer(state, payload) {
    var parent = payload.parent ? payload.parent.split('-') : [];
    return updateState(state, (0, _objectSpread3.default)({}, payload, {
      parent: parent
    }));
  });
  var initialStateRef = (0, _react.useRef)();
  var mapInitialState = (0, _objectSpread3.default)({}, modelDefaults(), {
    fields: buildModel({
      fields: initialState.fields
    })
  });

  var _useReducer = (0, _react.useReducer)(reducer, mapInitialState),
      _useReducer2 = (0, _slicedToArray2.default)(_useReducer, 2),
      model = _useReducer2[0],
      dispatch = _useReducer2[1];

  (0, _isEqualEffect.useIsEqualEffect)(function () {
    dispatch({
      type: 'FORM_REPLACE',
      model: (0, _objectSpread3.default)({}, initialState, {
        fields: buildModel({
          fields: initialState.fields
        })
      })
    });
    initialStateRef.current = initialState;
  }, [initialState, initialStateRef.current]);

  function handleBlur(e) {
    var _e$target = e.target,
        id = _e$target.id,
        parent = _e$target.dataset.parent;
    var field = getField(function (result) {
      return result;
    })({
      id: id,
      parent: parent,
      fields: model.fields
    });
    field.data.attributes && validator({
      id: id,
      parent: parent,
      model: model,
      dispatch: dispatch
    });
    onBlur && onBlur(e, {
      field: field,
      model: model,
      dispatch: dispatch
    });
  }

  function handleFocus(e) {
    onFocus && onFocus(e, {
      field: field,
      model: model,
      dispatch: dispatch
    });
  }

  function handleChange(e) {
    var _e$target2 = e.target,
        id = _e$target2.id,
        parent = _e$target2.dataset.parent;
    var field = getField(id);
    var data = {
      text: {
        value: e.target.value
      },
      checkbox: {
        checked: e.target.checked
      },
      radio: {
        value: e.target.value
      }
    }[e.target.type] || {
      value: e.target.value
    };
    onChange && onChange(e, {
      field: field,
      model: model,
      dispatch: dispatch
    });
    dispatch({
      type: 'FORM_UPDATE',
      attributes: data,
      id: id,
      parent: parent
    });
  }

  function handleSubmit(e) {
    e.preventDefault();
    var isValid = true;

    var getValues = function getValues(fields) {
      return fields.reduce(function (previous, _ref3) {
        var _ref3$attributes = _ref3.attributes,
            checked = _ref3$attributes.checked,
            name = _ref3$attributes.name,
            value = _ref3$attributes.value,
            children = _ref3.children,
            id = _ref3.id,
            parent = _ref3.parent;

        var currentValue = function currentValue() {
          return typeof checked !== 'undefined' ? checked : value;
        };

        var values = name ? (0, _objectSpread3.default)({}, previous.values, (0, _defineProperty2.default)({}, name, children ? (0, _objectSpread3.default)({}, previous.values[name], getValues(children)) : currentValue())) : previous.values;
        var isValid = validator({
          id: id,
          parent: parent,
          model: model,
          dispatch: dispatch
        });
        var errors = [].concat((0, _toConsumableArray2.default)(previous.errors), (0, _toConsumableArray2.default)(!isValid ? [{
          name: name,
          id: id
        }] : []));
        return {
          values: values,
          errors: errors
        };
      }, {
        values: {},
        errors: []
      });
    };

    onSubmit(getValues(model.fields), model);
  }

  return {
    dispatch: dispatch,
    model: model,
    handleChange: handleChange,
    handleBlur: handleBlur,
    handleSubmit: handleSubmit
  };
}

function buildModel(_ref4) {
  var fields = _ref4.fields,
      _ref4$parent = _ref4.parent,
      parent = _ref4$parent === void 0 ? {} : _ref4$parent;
  return fields.map(function (field) {
    var attributes = field.attributes,
        id = field.id,
        isValid = field.isValid,
        name = field.name,
        touched = field.touched;
    var obj = (0, _objectSpread3.default)({}, field, {
      attributes: (0, _objectSpread3.default)({}, attributes, {
        name: name || attributes && attributes.name || parent.name
      })
    }, modelDefaults({
      id: id,
      isValid: isValid,
      parent: parent,
      touched: touched
    }));
    return field.children ? (0, _objectSpread3.default)({}, obj, {
      children: buildModel({
        fields: field.children,
        parent: field
      })
    }) : obj;
  });
}

var getField = (0, _curry2.default)(function getField(cb, _ref5) {
  var fields = _ref5.fields,
      id = _ref5.id,
      _ref5$parent = _ref5.parent,
      parent = _ref5$parent === void 0 ? [] : _ref5$parent;
  var parentArray = Array.isArray(parent) ? parent : parent.split('-');
  var parentId = parent.length > 0 && parentArray[0].trim() !== '' && parentArray[0];
  var field = fields.filter(function (item) {
    return item.id === (parentId || id);
  })[0] || {};
  var index = fields.findIndex(function (item) {
    return item.id === (parentId || id);
  });
  return field.children ? getField(cb, {
    fields: field.children,
    parent: parentArray.splice(1, parent.length),
    id: id
  }) : cb({
    data: field,
    index: index
  });
});

function updateState(_ref7, _ref6) {
  var fields = _ref7.fields,
      state = (0, _objectWithoutProperties2.default)(_ref7, ["fields"]);
  var type = _ref6.type,
      attributes = _ref6.attributes,
      id = _ref6.id,
      model = _ref6.model,
      parent = _ref6.parent,
      dataRest = (0, _objectWithoutProperties2.default)(_ref6, ["type", "attributes", "id", "model", "parent"]);

  switch (type) {
    case 'FORM_REPLACE':
      return model;

    case 'FORM_UPDATE':
      {
        var parentId = parent.length > 0 && parent[0].trim() !== '' && parent[0];
        var _field = fields.filter(function (item) {
          return item.id === (parentId || id);
        })[0];
        var index = fields.findIndex(function (item) {
          return item.id === (parentId || id);
        });

        var parentData = function parentData() {
          var children = _field.children ? parent ? updateState({
            fields: _field.children
          }, (0, _objectSpread3.default)({
            type: 'FORM_UPDATE'
          }, dataRest, {
            attributes: attributes,
            id: id,
            parent: parent.splice(1, parent.length)
          })) : _field.children : {};
          return (0, _objectSpread3.default)({}, _field, children && {
            children: children
          }, {
            dirty: true,
            touched: true
          });
        };

        var fieldData = function fieldData() {
          return (0, _objectSpread3.default)({}, _field, dataRest, {
            attributes: (0, _objectSpread3.default)({}, _field.attributes, attributes),
            dirty: true,
            touched: true
          });
        };

        var data = parent.length > 0 ? parentData() : fieldData();

        var isUndefined = function isUndefined(value) {
          return typeof value === 'undefined';
        };

        return (0, _objectSpread3.default)({}, state, {
          dirty: isUndefined(dataRest.dirty) ? state.dirty : true,
          touched: isUndefined(dataRest.touched) ? state.touched : true,
          isValid: isUndefined(dataRest.isValid) ? state.isValid : dataRest.isValid,
          fields: index === 0 ? [data].concat((0, _toConsumableArray2.default)(fields.slice(1, fields.length))) : index === state.length - 1 ? [].concat((0, _toConsumableArray2.default)(fields.slice(0, fields.length - 1)), [data]) : [].concat((0, _toConsumableArray2.default)(fields.slice(0, index)), [data], (0, _toConsumableArray2.default)(fields.slice(index + 1, fields.length)))
        });
      }

    default:
      return state;
  }
}

function validator(_ref8) {
  var dispatch = _ref8.dispatch,
      id = _ref8.id,
      model = _ref8.model,
      parent = _ref8.parent;
  var field = getField(function (_ref9) {
    var data = _ref9.data;
    return data;
  })({
    id: id,
    parent: parent,
    fields: model.fields
  });
  if (!field) return false;
  var attributes = field.attributes,
      isValid = field.isValid,
      fieldValidation = field.validate;

  var _ref10 = field.attributes || {},
      _ref10$value = _ref10.value,
      value = _ref10$value === void 0 ? '' : _ref10$value,
      required = _ref10.required,
      _ref10$type = _ref10.type,
      type = _ref10$type === void 0 ? 'text' : _ref10$type,
      checked = _ref10.checked;

  var payload = {
    id: id,
    parent: parent
  };
  var hasChecked = checked == null;
  var hasValue = value || value.trim() !== '';
  var notValid = required && hasChecked && !hasValue || fieldValidation && fieldValidation(field, model, dispatch) && hasValue;

  if (!notValid !== isValid) {
    dispatch((0, _objectSpread3.default)({
      type: 'FORM_UPDATE'
    }, payload, {
      isValid: !notValid
    }));
  }

  return !notValid;
}