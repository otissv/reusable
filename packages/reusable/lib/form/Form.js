"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Form = exports.Fields = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _useForm2 = require("./useForm");

//TODO: add hide booleam / function
//TODO: add error commponent
var Fields = (0, _react.memo)(function Fields(_ref) {
  var handleBlur = _ref.handleBlur,
      handleChange = _ref.handleChange,
      model = _ref.model,
      parent = _ref.parent;
  return model.map(function (_ref2) {
    var _ref2$attributes = _ref2.attributes,
        attributes = _ref2$attributes === void 0 ? {} : _ref2$attributes,
        children = _ref2.children,
        component = _ref2.component,
        heading = _ref2.heading,
        id = _ref2.id;
    var label = attributes.label,
        attributesRest = (0, _objectWithoutProperties2.default)(attributes, ["label"]);
    var render = component ? component : children ? _react.default.createElement("div", attributesRest, Fields({
      handleBlur: handleBlur,
      handleChange: handleChange,
      model: children,
      parent: id
    })) : _react.default.createElement(_react.Fragment, null, _react.default.createElement("label", {
      key: id,
      htmlFor: id
    }, label), _react.default.createElement("input", (0, _extends2.default)({
      id: id,
      "data-parent": parent,
      onBlur: handleBlur,
      onChange: handleChange
    }, attributesRest)));
    return _react.default.createElement(_react.Fragment, {
      key: id
    }, heading && _react.default.createElement("div", null, heading(model)), render);
  });
});
exports.Fields = Fields;
var Form = (0, _react.memo)(function Form(_ref3) {
  var children = _ref3.children,
      initialState = _ref3.initialState,
      onChange = _ref3.onChange,
      onSubmit = _ref3.onSubmit;

  var _useForm = (0, _useForm2.useForm)({
    initialState: initialState,
    onSubmit: onSubmit,
    onChange: onChange
  }),
      dispatch = _useForm.dispatch,
      model = _useForm.model,
      handleChange = _useForm.handleChange,
      handleBlur = _useForm.handleBlur,
      handleSubmit = _useForm.handleSubmit;

  var render = typeof children === 'function' ? children({
    dispatch: dispatch,
    model: model,
    handleChange: handleChange
  }) : children;
  return _react.default.createElement("form", {
    onSubmit: handleSubmit
  }, _react.default.createElement(Fields, {
    model: model,
    handleChange: handleChange,
    handleBlur: handleBlur
  }), render);
});
exports.Form = Form;