"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _collectionReducer = require("./collectionReducer");

var collections = [{
  id: 1,
  firstName: '1 firstName',
  lastName: '1 lastName',
  score: 1
}, {
  id: 2,
  firstName: '2 firstName',
  lastName: '2 lastName',
  score: 2
}, {
  id: 3,
  firstName: '3 firstName',
  lastName: '3 lastName',
  score: 3
}];
describe('bulk', function () {
  it('Should insert many items into collection', function () {
    var data = [{
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4
    }, {
      id: 5,
      firstName: '5 firstName',
      lastName: '5 lastName',
      score: 5
    }];
    var actual = (0, _collectionReducer.bulkInsert)(collections)()(data);
    var expected = [].concat(collections, data);
    expect(actual).toEqual(expected);
  });
  it('Should remove items in bulk', function () {
    var queries = [function (i) {
      return i.id === 1;
    }, function (i) {
      return i.id === 2;
    }];
    var actual = (0, _collectionReducer.bulkRemove)([{
      id: 1
    }, {
      id: 2
    }, {
      id: 3
    }, {
      id: 4
    }])(queries);
    var expected = [{
      id: 3
    }, {
      id: 4
    }];
    expect(actual).toEqual(expected);
  });
  it('Should update items in bulk', function () {
    var queries = [{
      query: function query(i) {
        return i.id === 1;
      },
      data: {
        firstName: 1
      }
    }, {
      query: function query(i) {
        return i.id === 2;
      },
      data: {
        firstName: 2
      }
    }, {
      query: function query(i) {
        return i.id === 3;
      },
      data: {
        firstName: 3
      }
    }];
    var actual = (0, _collectionReducer.bulkUpdate)(collections)(queries);
    var expected = [{
      id: 1,
      firstName: 1,
      lastName: '1 lastName',
      score: 1
    }, {
      id: 2,
      firstName: 2,
      lastName: '2 lastName',
      score: 2
    }, {
      id: 3,
      firstName: 3,
      lastName: '3 lastName',
      score: 3
    }];
    expect(actual).toEqual(expected);
  });
});
describe('find', function () {
  it('Should not find items in collection', function () {
    var actual = (0, _collectionReducer.find)(collections)();
    expect(actual).toEqual([]);
  });
  it('Should find items in collection', function () {
    var query = function query(item) {
      return item.score > 1;
    };

    var actual = (0, _collectionReducer.find)(collections)(query);
    var expected = [{
      id: 2,
      firstName: '2 firstName',
      lastName: '2 lastName',
      score: 2
    }, {
      id: 3,
      firstName: '3 firstName',
      lastName: '3 lastName',
      score: 3
    }];
    expect(actual).toEqual(expected);
  });
});
describe('find index', function () {
  it('Should find index of item in collection', function () {
    var query = function query(item) {
      return item.id === 1;
    };

    var actual = (0, _collectionReducer.findIndex)(collections)(query);
    var expected = 0;
    expect(actual).toBe(expected);
  });
  it('Should not find index of item in collection', function () {
    var query = function query(item) {
      return item.id === 0;
    };

    var actual = (0, _collectionReducer.findIndex)(collections)(query);
    var expected = -1;
    expect(actual).toBe(expected);
  });
});
describe('find one', function () {
  it('Should return undefined if no query', function () {
    var actual = (0, _collectionReducer.findOne)(collections)();
    expect(actual).toEqual(undefined);
  });
  it('Should find one item in collection', function () {
    var query = function query(c) {
      return c.id === 1;
    };

    var actual = (0, _collectionReducer.findOne)(collections)(query);
    var expected = {
      id: 1,
      firstName: '1 firstName',
      lastName: '1 lastName',
      score: 1
    };
    expect(actual).toEqual(expected);
  });
});
describe('insert', function () {
  it('Should insert item into collection', function () {
    var data = {
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4
    };
    var actual = (0, _collectionReducer.insert)(collections)()(data);
    var expected = [].concat(collections, [data]);
    expect(actual).toEqual(expected);
  });
  it('Should insert item into collection at index', function () {
    var data = {
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4
    };
    var actual = (0, _collectionReducer.insert)(collections)(0)(data);
    var expected = [data].concat(collections);
    expect(actual).toEqual(expected);
  });
});
describe('remove', function () {
  it('Should remove item in collection', function () {
    var actual = (0, _collectionReducer.remove)(collections)(function (i) {
      return i.score > 1;
    });
    var expected = [{
      id: 1,
      firstName: '1 firstName',
      lastName: '1 lastName',
      score: 1
    }];
    expect(actual).toEqual(expected);
  });
});
describe('update', function () {
  it('Should update item in collection', function () {
    var data = {
      firstName: 'updated'
    };

    var query = function query(i) {
      return i.id === 1;
    };

    var actual = (0, _collectionReducer.update)(collections)(query)(data);
    var expected = [{
      id: 1,
      firstName: 'updated',
      lastName: '1 lastName',
      score: 1
    }, {
      id: 2,
      firstName: '2 firstName',
      lastName: '2 lastName',
      score: 2
    }, {
      id: 3,
      firstName: '3 firstName',
      lastName: '3 lastName',
      score: 3
    }];
    expect(actual).toEqual(expected);
  });
  it('Should update multiple items in collection', function () {
    var data = {
      score: 0
    };
    var actual = (0, _collectionReducer.update)(collections)(function (i) {
      return i.score > 1;
    })(data);
    var expected = [{
      id: 1,
      firstName: '1 firstName',
      lastName: '1 lastName',
      score: 1
    }, {
      id: 2,
      firstName: '2 firstName',
      lastName: '2 lastName',
      score: 0
    }, {
      id: 3,
      firstName: '3 firstName',
      lastName: '3 lastName',
      score: 0
    }];
    expect(actual).toEqual(expected);
  });
});
describe('reducer', function () {
  it('Should update items in bulk', function () {
    var action = {
      type: 'BULK_UPDATE',
      query: [{
        query: function query(i) {
          return i.id === 1;
        },
        data: {
          firstName: 1
        }
      }, {
        query: function query(i) {
          return i.id === 2;
        },
        data: {
          firstName: 2
        }
      }, {
        query: function query(i) {
          return i.id === 3;
        },
        data: {
          firstName: 3
        }
      }]
    };
    var crud = {
      bulkUpdate: _collectionReducer.bulkUpdate
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    var expected = {
      collections: [{
        id: 1,
        firstName: 1,
        lastName: '1 lastName',
        score: 1
      }, {
        id: 2,
        firstName: 2,
        lastName: '2 lastName',
        score: 2
      }, {
        id: 3,
        firstName: 3,
        lastName: '3 lastName',
        score: 3
      }]
    };
    expect(actual).toEqual(expected);
  });
  it('Should remove items in bulk', function () {
    var action = {
      type: 'BULK_REMOVE',
      query: [function (i) {
        return i.id === 1;
      }, function (i) {
        return i.id === 2;
      }]
    };
    var crud = {
      bulkRemove: _collectionReducer.bulkRemove
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    var expected = {
      collections: collections.slice(2, collections.length)
    };
    expect(actual).toEqual(expected);
  }); // it('Should not find items in collection', () => {
  //   const action = { type: 'FIND' }
  //   const crud = { find }
  //   const actual = reducer({ collections }, action, crud)
  //   expect(actual).toEqual({ collections: [] })
  // })
  // it('Should find items in collection', () => {
  //   const action = { type: 'FIND', query: item => item.score > 1 }
  //   const crud = { find }
  //   const actual = reducer({ collections }, action, crud)
  //   const expected = {
  //     collections: [
  //       {
  //         id: 2,
  //         firstName: '2 firstName',
  //         lastName: '2 lastName',
  //         score: 2,
  //       },
  //       {
  //         id: 3,
  //         firstName: '3 firstName',
  //         lastName: '3 lastName',
  //         score: 3,
  //       },
  //     ],
  //   }
  //   expect(actual).toEqual(expected)
  // })
  // it('Should return undefined if no query', () => {
  //   const action = { type: 'FIND_ONE' }
  //   const crud = { findOne }
  //   const actual = reducer({ collections }, action, crud)
  //   expect(actual).toEqual(undefined)
  // })

  it('Should select an item in collection', function () {
    var action = {
      type: 'SELECT',
      query: function query(c) {
        return c.id === 1;
      }
    };
    var crud = {
      findOne: _collectionReducer.findOne
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections,
      selected: null
    }, action, crud);
    var expected = {
      collections: collections,
      selected: {
        id: 1,
        firstName: '1 firstName',
        lastName: '1 lastName',
        score: 1
      }
    };
    expect(actual).toEqual(expected);
  });
  it('Should insert item into collection', function () {
    var data = {
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4
    };
    var action = (0, _objectSpread2.default)({
      type: 'INSERT'
    }, data);
    var crud = {
      insert: _collectionReducer.insert
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    var expected = {
      collections: [].concat(collections, [data])
    };
    expect(actual).toEqual(expected);
  });
  it('Should insert item into collection at index', function () {
    var data = {
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4
    };
    var action = (0, _objectSpread2.default)({
      type: 'INSERT',
      index: 0
    }, data);
    var crud = {
      insert: _collectionReducer.insert
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    var expected = {
      collections: [data].concat(collections)
    };
    expect(actual).toEqual(expected);
  });
  it('Should insert many items into collection', function () {
    var data = [{
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4
    }, {
      id: 5,
      firstName: '5 firstName',
      lastName: '5 lastName',
      score: 5
    }];
    var action = (0, _objectSpread2.default)({
      type: 'BULK_INSERT'
    }, data);
    var crud = {
      bulkInsert: _collectionReducer.bulkInsert
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    var expected = {
      collections: [].concat(collections, data)
    };
    expect(actual).toEqual(expected);
  });
  it('Should insert many items into collection at index', function () {
    var data = [{
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4
    }, {
      id: 5,
      firstName: '5 firstName',
      lastName: '5 lastName',
      score: 5
    }];
    var action = (0, _objectSpread2.default)({
      type: 'BULK_INSERT',
      index: 0
    }, data);
    var crud = {
      bulkInsert: _collectionReducer.bulkInsert
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    var expected = {
      collections: [].concat(data, collections)
    };
    expect(actual).toEqual(expected);
  });
  it('Should remove item in collection', function () {
    var action = {
      type: 'REMOVE',
      query: function query(i) {
        return i.score > 1;
      }
    };
    var crud = {
      remove: _collectionReducer.remove
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    var expected = {
      collections: [{
        id: 1,
        firstName: '1 firstName',
        lastName: '1 lastName',
        score: 1
      }]
    };
    expect(actual).toEqual(expected);
  });
  it('Should update item in collection', function () {
    var data = {
      firstName: 'updated'
    };
    var action = (0, _objectSpread2.default)({
      type: 'UPDATE',
      query: function query(i) {
        return i.id === 1;
      }
    }, data);
    var crud = {
      update: _collectionReducer.update
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    var expected = {
      collections: [{
        id: 1,
        firstName: 'updated',
        lastName: '1 lastName',
        score: 1
      }, {
        id: 2,
        firstName: '2 firstName',
        lastName: '2 lastName',
        score: 2
      }, {
        id: 3,
        firstName: '3 firstName',
        lastName: '3 lastName',
        score: 3
      }]
    };
    expect(actual).toEqual(expected);
  });
  it('Should update multiple items in collection', function () {
    var data = {
      score: 0
    };
    var action = (0, _objectSpread2.default)({
      type: 'UPDATE',
      query: function query(i) {
        return i.score > 1;
      }
    }, data);
    var crud = {
      update: _collectionReducer.update
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    var expected = {
      collections: [{
        id: 1,
        firstName: '1 firstName',
        lastName: '1 lastName',
        score: 1
      }, {
        id: 2,
        firstName: '2 firstName',
        lastName: '2 lastName',
        score: 0
      }, {
        id: 3,
        firstName: '3 firstName',
        lastName: '3 lastName',
        score: 0
      }]
    };
    expect(actual).toEqual(expected);
  });
  it('Should replace all items in collection', function () {
    var data = {
      collections: [{
        id: 0,
        firstName: '0 firstName',
        lastName: '0 lastName',
        score: 0
      }]
    };
    var crud = {
      remove: _collectionReducer.remove
    };
    var action = (0, _objectSpread2.default)({
      type: 'REPLACE'
    }, data);
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud);
    expect(actual).toEqual(data);
  });
  it('Should reset collection to initial state', function () {
    var initialState = {
      collections: [{
        id: 0,
        firstName: '0 firstName',
        lastName: '0 lastName',
        score: 0
      }]
    };
    var crud = {
      remove: _collectionReducer.remove
    };
    var action = {
      type: 'RESET'
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud, initialState);
    expect(actual).toEqual(initialState);
  });
  it('Should  extend reducer', function () {
    function extendReducer(state, action, _ref) {
      var update = _ref.update;

      switch (action.type) {
        case 'RESET_SCORES':
          {
            var data = {
              score: 0
            };

            var query = function query(item) {
              return item.score > 0;
            };

            return {
              collections: update(state.collections)(query)(data)
            };
          }

        default:
          return state;
      }
    }

    var crud = {
      update: _collectionReducer.update
    };
    var action = {
      type: 'RESET_SCORES'
    };
    var actual = (0, _collectionReducer.reducer)({
      collections: collections
    }, action, crud, [], extendReducer);
    var expected = {
      collections: [{
        firstName: '1 firstName',
        id: 1,
        lastName: '1 lastName',
        score: 0
      }, {
        firstName: '2 firstName',
        id: 2,
        lastName: '2 lastName',
        score: 0
      }, {
        firstName: '3 firstName',
        id: 3,
        lastName: '3 lastName',
        score: 0
      }]
    };
    expect(actual).toEqual(expected);
  });
});