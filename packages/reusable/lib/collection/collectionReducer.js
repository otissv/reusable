"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bulkInsert = bulkInsert;
exports.bulkRemove = bulkRemove;
exports.bulkUpdate = bulkUpdate;
exports.find = find;
exports.findIndex = findIndex;
exports.findOne = findOne;
exports.insert = insert;
exports.remove = remove;
exports.update = update;
exports.reducer = reducer;
exports.useCollectionReducer = useCollectionReducer;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _react = require("react");

var _collectionConstants = require("./collectionConstants");

function bulkInsert(list) {
  return function (index) {
    return function (data) {
      if (!index && index !== 0) return [].concat((0, _toConsumableArray2.default)(list), (0, _toConsumableArray2.default)(data));
      return index === 0 ? [].concat((0, _toConsumableArray2.default)(data), (0, _toConsumableArray2.default)(list)) : index === list.length - 1 ? [].concat((0, _toConsumableArray2.default)(list), (0, _toConsumableArray2.default)(data)) : [].concat((0, _toConsumableArray2.default)(list.slice(0, index)), (0, _toConsumableArray2.default)(data), (0, _toConsumableArray2.default)(list.slice(index)));
    };
  };
}

function bulkRemove(list) {
  return function (queries) {
    return queries.reduce(function (previous, query) {
      return previous.filter(function (item) {
        return !query(item);
      });
    }, list);
  };
}

function bulkUpdate(list) {
  return function (updates) {
    return updates.reduce(function (previous, update) {
      return previous.map(function (item) {
        return update.query(item) ? (0, _objectSpread2.default)({}, item, update.data) : item;
      });
    }, list);
  };
}

function find(list) {
  return function (query) {
    return query ? list.filter(query) : [];
  };
}

function findIndex(list) {
  return function (query) {
    return list.findIndex(query);
  };
}

function findOne(list) {
  return function (query) {
    return query ? list.filter(query)[0] : undefined;
  };
}

function insert(list) {
  return function (index) {
    return function (data) {
      if (!index && index !== 0) return [].concat((0, _toConsumableArray2.default)(list), [data]);
      return index === 0 ? [data].concat((0, _toConsumableArray2.default)(list)) : index === list.length - 1 ? [].concat((0, _toConsumableArray2.default)(list), [data]) : [].concat((0, _toConsumableArray2.default)(list.slice(0, index)), [data], (0, _toConsumableArray2.default)(list.slice(index)));
    };
  };
}

function remove(list) {
  return function (query) {
    return list.reduce(function (accumulator, item) {
      return query(item) ? accumulator : [].concat((0, _toConsumableArray2.default)(accumulator), [item]);
    }, []);
  }; // return query => {
  //   const index = findIndex(list)(query)
  //   return index === 0
  //     ? list.slice(1)
  //     : index === list.length - 1
  //     ? list.slice(0, list.length - 1)
  //     : [...list.slice(0, index), ...list.slice(index + 1)]
  // }
}

function update(list) {
  return function (query) {
    return function (data) {
      return list.map(function (item) {
        return query(item) ? (0, _objectSpread2.default)({}, item, data) : item;
      });
    };
  };
}

function reducer(state, action, crud, initialState, extendReducer) {
  var insert = crud.insert,
      bulkInsert = crud.bulkInsert,
      bulkRemove = crud.bulkRemove,
      bulkUpdate = crud.bulkUpdate,
      find = crud.find,
      findOne = crud.findOne,
      remove = crud.remove,
      update = crud.update;
  var type = action.type,
      index = action.index,
      query = action.query,
      data = (0, _objectWithoutProperties2.default)(action, ["type", "index", "query"]);

  switch (action.type) {
    case 'BULK_INSERT':
      {
        var payload = Object.keys(data).reduce(function (previous, key) {
          return [].concat((0, _toConsumableArray2.default)(previous), [data[key]]);
        }, []);
        return (0, _objectSpread2.default)({}, state, {
          collections: bulkInsert(state.collections)(action.index)(payload)
        });
      }

    case 'BULK_REMOVE':
      return (0, _objectSpread2.default)({}, state, {
        collections: bulkRemove(state.collections)(action.query)
      });

    case 'BULK_UPDATE':
      return (0, _objectSpread2.default)({}, state, {
        collections: bulkUpdate(state.collections)(action.query)
      });

    case 'CLEAR':
      return [];

    case 'INSERT':
      return (0, _objectSpread2.default)({}, state, {
        collections: insert(state.collections)(action.index)(data)
      });

    case 'UPDATE':
      return (0, _objectSpread2.default)({}, state, {
        collections: update(state.collections)(action.query)(data)
      });

    case 'REMOVE':
      return (0, _objectSpread2.default)({}, state, {
        collections: remove(state.collections)(action.query)
      });

    case 'REPLACE':
      return (0, _objectSpread2.default)({}, state, data);

    case 'RESET':
      return initialState;

    case 'SELECT':
      return (0, _objectSpread2.default)({}, state, {
        selected: findOne(state.collections)(action.query)
      });

    default:
      return extendReducer ? extendReducer(state, action, crud) : state;
  }
}

function useCollectionReducer(extendReducer, initialState) {
  var initialValue = (0, _objectSpread2.default)({}, _collectionConstants.INITIAL_STATE, initialState);

  function crudReducer(state, action) {
    var crud = {
      bulkInsert: bulkInsert,
      bulkRemove: bulkRemove,
      bulkUpdate: bulkUpdate,
      find: find,
      findIndex: findIndex,
      findOne: findOne,
      insert: insert,
      remove: remove,
      update: update
    };
    return reducer(state, action, crud, initialValue, extendReducer);
  }

  return (0, _react.useReducer)(crudReducer, initialValue);
}