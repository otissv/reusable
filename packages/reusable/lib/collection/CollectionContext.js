"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CollectionProvider = CollectionProvider;
exports.CollectionContext = void 0;

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _react = _interopRequireDefault(require("react"));

var _collectionReducer = require("./collectionReducer");

var _devtools = require("../devtools");

var CollectionContext = _react.default.createContext([]);

exports.CollectionContext = CollectionContext;

function CollectionProvider(_ref) {
  var children = _ref.children,
      context = _ref.context,
      value = _ref.value,
      name = _ref.name,
      reducer = _ref.reducer;
  var initialState = (0, _objectSpread2.default)({
    collections: [],
    selected: null
  }, value);
  var collectionReducer = (0, _collectionReducer.useCollectionReducer)(reducer, initialState);

  var _useReducerDevtools = (0, _devtools.useReducerDevtools)(collectionReducer, initialState, name),
      _useReducerDevtools2 = (0, _slicedToArray2.default)(_useReducerDevtools, 2),
      state = _useReducerDevtools2[0],
      dispatch = _useReducerDevtools2[1];

  var Context = context || CollectionContext;
  return _react.default.createElement(Context.Provider, {
    value: [state, dispatch]
  }, children);
}