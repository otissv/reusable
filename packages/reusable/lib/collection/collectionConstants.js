"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.INITIAL_STATE = void 0;
var INITIAL_STATE = {
  collections: [],
  selected: null,
  collectionsLoading: false,
  selectedLoading: false
};
exports.INITIAL_STATE = INITIAL_STATE;