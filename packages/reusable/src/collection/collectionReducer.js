import { useReducer } from 'react'
import { INITIAL_STATE } from './collectionConstants'

export function bulkInsert(list) {
  return index => data => {
    if (!index && index !== 0) return [...list, ...data]

    return index === 0
      ? [...data, ...list]
      : index === list.length - 1
      ? [...list, ...data]
      : [...list.slice(0, index), ...data, ...list.slice(index)]
  }
}

export function bulkRemove(list) {
  return queries =>
    queries.reduce(
      (previous, query) => previous.filter(item => !query(item)),
      list
    )
}

export function bulkUpdate(list) {
  return updates =>
    updates.reduce(
      (previous, update) =>
        previous.map(item =>
          update.query(item) ? { ...item, ...update.data } : item
        ),
      list
    )
}

export function find(list) {
  return query => (query ? list.filter(query) : [])
}

export function findIndex(list) {
  return query => list.findIndex(query)
}

export function findOne(list) {
  return query => (query ? list.filter(query)[0] : undefined)
}

export function insert(list) {
  return index => data => {
    if (!index && index !== 0) return [...list, data]

    return index === 0
      ? [data, ...list]
      : index === list.length - 1
      ? [...list, data]
      : [...list.slice(0, index), data, ...list.slice(index)]
  }
}

export function remove(list) {
  return query =>
    list.reduce(
      (accumulator, item) =>
        query(item) ? accumulator : [...accumulator, item],
      []
    )
  // return query => {
  //   const index = findIndex(list)(query)

  //   return index === 0
  //     ? list.slice(1)
  //     : index === list.length - 1
  //     ? list.slice(0, list.length - 1)
  //     : [...list.slice(0, index), ...list.slice(index + 1)]
  // }
}

export function update(list) {
  return query => data =>
    list.map(item => (query(item) ? { ...item, ...data } : item))
}

export function reducer(state, action, crud, initialState, extendReducer) {
  const {
    insert,
    bulkInsert,
    bulkRemove,
    bulkUpdate,
    find,
    findOne,
    remove,
    update,
  } = crud
  const { type, index, query, ...data } = action

  switch (action.type) {
    case 'BULK_INSERT': {
      const payload = Object.keys(data).reduce(
        (previous, key) => [...previous, data[key]],
        []
      )
      return {
        ...state,
        collections: bulkInsert(state.collections)(action.index)(payload),
      }
    }

    case 'BULK_REMOVE':
      return {
        ...state,

        collections: bulkRemove(state.collections)(action.query),
      }

    case 'BULK_UPDATE':
      return {
        ...state,

        collections: bulkUpdate(state.collections)(action.query),
      }

    case 'CLEAR':
      return []

    case 'INSERT':
      return {
        ...state,
        collections: insert(state.collections)(action.index)(data),
      }

    case 'UPDATE':
      return {
        ...state,
        collections: update(state.collections)(action.query)(data),
      }

    case 'REMOVE':
      return {
        ...state,
        collections: remove(state.collections)(action.query),
      }

    case 'REPLACE':
      return {
        ...state,
        ...data,
      }

    case 'RESET':
      return initialState

    case 'SELECT':
      return {
        ...state,
        selected: findOne(state.collections)(action.query),
      }
    default:
      return extendReducer ? extendReducer(state, action, crud) : state
  }
}

export function useCollectionReducer(extendReducer, initialState) {
  const initialValue = {
    ...INITIAL_STATE,
    ...initialState,
  }

  function crudReducer(state, action) {
    const crud = {
      bulkInsert,
      bulkRemove,
      bulkUpdate,
      find,
      findIndex,
      findOne,
      insert,
      remove,
      update,
    }

    return reducer(state, action, crud, initialValue, extendReducer)
  }

  return useReducer(crudReducer, initialValue)
}
