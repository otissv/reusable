import React from 'react'
import { useCollectionReducer } from './collectionReducer'
import { useReducerDevtools } from '../devtools'

export const CollectionContext = React.createContext([])

export function CollectionProvider({
  children,
  context,
  value,
  name,
  reducer,
}) {
  const initialState = { collections: [], selected: null, ...value }
  const collectionReducer = useCollectionReducer(reducer, initialState)
  const [state, dispatch] = useReducerDevtools(
    collectionReducer,
    initialState,
    name
  )
  const Context = context || CollectionContext

  return (
    <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>
  )
}
