import {
  bulkInsert,
  bulkRemove,
  bulkUpdate,
  find,
  findIndex,
  findOne,
  insert,
  reducer,
  remove,
  update,
} from './collectionReducer'

const collections = [
  {
    id: 1,
    firstName: '1 firstName',
    lastName: '1 lastName',
    score: 1,
  },
  {
    id: 2,
    firstName: '2 firstName',
    lastName: '2 lastName',
    score: 2,
  },
  {
    id: 3,
    firstName: '3 firstName',
    lastName: '3 lastName',
    score: 3,
  },
]

describe('bulk', () => {
  it('Should insert many items into collection', () => {
    const data = [
      {
        id: 4,
        firstName: '4 firstName',
        lastName: '4 lastName',
        score: 4,
      },
      {
        id: 5,
        firstName: '5 firstName',
        lastName: '5 lastName',
        score: 5,
      },
    ]

    const actual = bulkInsert(collections)()(data)
    const expected = [...collections, ...data]
    expect(actual).toEqual(expected)
  })

  it('Should remove items in bulk', () => {
    const queries = [i => i.id === 1, i => i.id === 2]
    const actual = bulkRemove([{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }])(
      queries
    )
    const expected = [{ id: 3 }, { id: 4 }]
    expect(actual).toEqual(expected)
  })

  it('Should update items in bulk', () => {
    const queries = [
      {
        query: i => i.id === 1,
        data: { firstName: 1 },
      },
      {
        query: i => i.id === 2,
        data: { firstName: 2 },
      },
      {
        query: i => i.id === 3,
        data: { firstName: 3 },
      },
    ]

    const actual = bulkUpdate(collections)(queries)
    const expected = [
      {
        id: 1,
        firstName: 1,
        lastName: '1 lastName',
        score: 1,
      },
      {
        id: 2,
        firstName: 2,
        lastName: '2 lastName',
        score: 2,
      },
      {
        id: 3,
        firstName: 3,
        lastName: '3 lastName',
        score: 3,
      },
    ]

    expect(actual).toEqual(expected)
  })
})

describe('find', () => {
  it('Should not find items in collection', () => {
    const actual = find(collections)()
    expect(actual).toEqual([])
  })

  it('Should find items in collection', () => {
    const query = item => item.score > 1

    const actual = find(collections)(query)
    const expected = [
      {
        id: 2,
        firstName: '2 firstName',
        lastName: '2 lastName',
        score: 2,
      },
      {
        id: 3,
        firstName: '3 firstName',
        lastName: '3 lastName',
        score: 3,
      },
    ]
    expect(actual).toEqual(expected)
  })
})

describe('find index', () => {
  it('Should find index of item in collection', () => {
    const query = item => item.id === 1
    const actual = findIndex(collections)(query)
    const expected = 0

    expect(actual).toBe(expected)
  })

  it('Should not find index of item in collection', () => {
    const query = item => item.id === 0
    const actual = findIndex(collections)(query)
    const expected = -1

    expect(actual).toBe(expected)
  })
})

describe('find one', () => {
  it('Should return undefined if no query', () => {
    const actual = findOne(collections)()
    expect(actual).toEqual(undefined)
  })

  it('Should find one item in collection', () => {
    const query = c => c.id === 1

    const actual = findOne(collections)(query)
    const expected = {
      id: 1,
      firstName: '1 firstName',
      lastName: '1 lastName',
      score: 1,
    }

    expect(actual).toEqual(expected)
  })
})

describe('insert', () => {
  it('Should insert item into collection', () => {
    const data = {
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4,
    }

    const actual = insert(collections)()(data)
    const expected = [...collections, data]
    expect(actual).toEqual(expected)
  })

  it('Should insert item into collection at index', () => {
    const data = {
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4,
    }

    const actual = insert(collections)(0)(data)
    const expected = [data, ...collections]
    expect(actual).toEqual(expected)
  })
})

describe('remove', () => {
  it('Should remove item in collection', () => {
    const actual = remove(collections)(i => i.score > 1)
    const expected = [
      {
        id: 1,
        firstName: '1 firstName',
        lastName: '1 lastName',
        score: 1,
      },
    ]
    expect(actual).toEqual(expected)
  })
})

describe('update', () => {
  it('Should update item in collection', () => {
    const data = { firstName: 'updated' }
    const query = i => i.id === 1

    const actual = update(collections)(query)(data)
    const expected = [
      {
        id: 1,
        firstName: 'updated',
        lastName: '1 lastName',
        score: 1,
      },
      {
        id: 2,
        firstName: '2 firstName',
        lastName: '2 lastName',
        score: 2,
      },
      {
        id: 3,
        firstName: '3 firstName',
        lastName: '3 lastName',
        score: 3,
      },
    ]

    expect(actual).toEqual(expected)
  })

  it('Should update multiple items in collection', () => {
    const data = { score: 0 }

    const actual = update(collections)(i => i.score > 1)(data)
    const expected = [
      {
        id: 1,
        firstName: '1 firstName',
        lastName: '1 lastName',
        score: 1,
      },
      {
        id: 2,
        firstName: '2 firstName',
        lastName: '2 lastName',
        score: 0,
      },
      {
        id: 3,
        firstName: '3 firstName',
        lastName: '3 lastName',
        score: 0,
      },
    ]

    expect(actual).toEqual(expected)
  })
})

describe('reducer', () => {
  it('Should update items in bulk', () => {
    const action = {
      type: 'BULK_UPDATE',
      query: [
        {
          query: i => i.id === 1,
          data: { firstName: 1 },
        },
        {
          query: i => i.id === 2,
          data: { firstName: 2 },
        },
        {
          query: i => i.id === 3,
          data: { firstName: 3 },
        },
      ],
    }
    const crud = { bulkUpdate }

    const actual = reducer({ collections }, action, crud)
    const expected = {
      collections: [
        {
          id: 1,
          firstName: 1,
          lastName: '1 lastName',
          score: 1,
        },
        {
          id: 2,
          firstName: 2,
          lastName: '2 lastName',
          score: 2,
        },
        {
          id: 3,
          firstName: 3,
          lastName: '3 lastName',
          score: 3,
        },
      ],
    }

    expect(actual).toEqual(expected)
  })

  it('Should remove items in bulk', () => {
    const action = {
      type: 'BULK_REMOVE',
      query: [i => i.id === 1, i => i.id === 2],
    }
    const crud = { bulkRemove }

    const actual = reducer({ collections }, action, crud)

    const expected = { collections: collections.slice(2, collections.length) }
    expect(actual).toEqual(expected)
  })

  // it('Should not find items in collection', () => {
  //   const action = { type: 'FIND' }
  //   const crud = { find }

  //   const actual = reducer({ collections }, action, crud)

  //   expect(actual).toEqual({ collections: [] })
  // })

  // it('Should find items in collection', () => {
  //   const action = { type: 'FIND', query: item => item.score > 1 }
  //   const crud = { find }

  //   const actual = reducer({ collections }, action, crud)
  //   const expected = {
  //     collections: [
  //       {
  //         id: 2,
  //         firstName: '2 firstName',
  //         lastName: '2 lastName',
  //         score: 2,
  //       },
  //       {
  //         id: 3,
  //         firstName: '3 firstName',
  //         lastName: '3 lastName',
  //         score: 3,
  //       },
  //     ],
  //   }

  //   expect(actual).toEqual(expected)
  // })

  // it('Should return undefined if no query', () => {
  //   const action = { type: 'FIND_ONE' }
  //   const crud = { findOne }

  //   const actual = reducer({ collections }, action, crud)
  //   expect(actual).toEqual(undefined)
  // })

  it('Should select an item in collection', () => {
    const action = { type: 'SELECT', query: c => c.id === 1 }
    const crud = { findOne }

    const actual = reducer({ collections, selected: null }, action, crud)
    const expected = {
      collections,
      selected: {
        id: 1,
        firstName: '1 firstName',
        lastName: '1 lastName',
        score: 1,
      },
    }

    expect(actual).toEqual(expected)
  })

  it('Should insert item into collection', () => {
    const data = {
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4,
    }

    const action = { type: 'INSERT', ...data }
    const crud = { insert }

    const actual = reducer({ collections }, action, crud)
    const expected = { collections: [...collections, data] }
    expect(actual).toEqual(expected)
  })

  it('Should insert item into collection at index', () => {
    const data = {
      id: 4,
      firstName: '4 firstName',
      lastName: '4 lastName',
      score: 4,
    }

    const action = { type: 'INSERT', index: 0, ...data }
    const crud = { insert }

    const actual = reducer({ collections }, action, crud)
    const expected = { collections: [data, ...collections] }
    expect(actual).toEqual(expected)
  })

  it('Should insert many items into collection', () => {
    const data = [
      {
        id: 4,
        firstName: '4 firstName',
        lastName: '4 lastName',
        score: 4,
      },
      {
        id: 5,
        firstName: '5 firstName',
        lastName: '5 lastName',
        score: 5,
      },
    ]

    const action = { type: 'BULK_INSERT', ...data }
    const crud = { bulkInsert }

    const actual = reducer({ collections }, action, crud)
    const expected = { collections: [...collections, ...data] }
    expect(actual).toEqual(expected)
  })

  it('Should insert many items into collection at index', () => {
    const data = [
      {
        id: 4,
        firstName: '4 firstName',
        lastName: '4 lastName',
        score: 4,
      },
      {
        id: 5,
        firstName: '5 firstName',
        lastName: '5 lastName',
        score: 5,
      },
    ]

    const action = { type: 'BULK_INSERT', index: 0, ...data }
    const crud = { bulkInsert }

    const actual = reducer({ collections }, action, crud)
    const expected = { collections: [...data, ...collections] }
    expect(actual).toEqual(expected)
  })

  it('Should remove item in collection', () => {
    const action = { type: 'REMOVE', query: i => i.score > 1 }
    const crud = { remove }
    const actual = reducer({ collections }, action, crud)

    const expected = {
      collections: [
        {
          id: 1,
          firstName: '1 firstName',
          lastName: '1 lastName',
          score: 1,
        },
      ],
    }

    expect(actual).toEqual(expected)
  })

  it('Should update item in collection', () => {
    const data = { firstName: 'updated' }

    const action = { type: 'UPDATE', query: i => i.id === 1, ...data }
    const crud = { update }
    const actual = reducer({ collections }, action, crud)

    const expected = {
      collections: [
        {
          id: 1,
          firstName: 'updated',
          lastName: '1 lastName',
          score: 1,
        },
        {
          id: 2,
          firstName: '2 firstName',
          lastName: '2 lastName',
          score: 2,
        },
        {
          id: 3,
          firstName: '3 firstName',
          lastName: '3 lastName',
          score: 3,
        },
      ],
    }

    expect(actual).toEqual(expected)
  })

  it('Should update multiple items in collection', () => {
    const data = { score: 0 }

    const action = { type: 'UPDATE', query: i => i.score > 1, ...data }
    const crud = { update }
    const actual = reducer({ collections }, action, crud)

    const expected = {
      collections: [
        {
          id: 1,
          firstName: '1 firstName',
          lastName: '1 lastName',
          score: 1,
        },
        {
          id: 2,
          firstName: '2 firstName',
          lastName: '2 lastName',
          score: 0,
        },
        {
          id: 3,
          firstName: '3 firstName',
          lastName: '3 lastName',
          score: 0,
        },
      ],
    }
    expect(actual).toEqual(expected)
  })

  it('Should replace all items in collection', () => {
    const data = {
      collections: [
        {
          id: 0,
          firstName: '0 firstName',
          lastName: '0 lastName',
          score: 0,
        },
      ],
    }
    const crud = { remove }
    const action = { type: 'REPLACE', ...data }

    const actual = reducer({ collections }, action, crud)

    expect(actual).toEqual(data)
  })

  it('Should reset collection to initial state', () => {
    const initialState = {
      collections: [
        {
          id: 0,
          firstName: '0 firstName',
          lastName: '0 lastName',
          score: 0,
        },
      ],
    }
    const crud = { remove }
    const action = { type: 'RESET' }

    const actual = reducer({ collections }, action, crud, initialState)

    expect(actual).toEqual(initialState)
  })

  it('Should  extend reducer', () => {
    function extendReducer(state, action, { update }) {
      switch (action.type) {
        case 'RESET_SCORES': {
          const data = { score: 0 }
          const query = item => item.score > 0
          return {
            collections: update(state.collections)(query)(data),
          }
        }
        default:
          return state
      }
    }
    const crud = { update }
    const action = { type: 'RESET_SCORES' }

    const actual = reducer({ collections }, action, crud, [], extendReducer)
    const expected = {
      collections: [
        { firstName: '1 firstName', id: 1, lastName: '1 lastName', score: 0 },
        { firstName: '2 firstName', id: 2, lastName: '2 lastName', score: 0 },
        { firstName: '3 firstName', id: 3, lastName: '3 lastName', score: 0 },
      ],
    }
    expect(actual).toEqual(expected)
  })
})
