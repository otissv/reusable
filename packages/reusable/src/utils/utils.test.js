import { Box, stringToArray, functionCallback } from './utils'

test('Box - is working', () => {
  const actual = Box('user')
    .map(str => str.toUpperCase())
    .map(str => `${str}_1`)
    .fold(str => str)

  expect(actual).toBe('USER_1')
})

test('stringToArray - Converts string to Array', () => {
  const actual = stringToArray('create read update delete')
  expect(actual).toEqual(['create', 'read', 'update', 'delete'])
})

test('stringToArray - returns Array if already an array', () => {
  const actual = stringToArray(['create', 'read', 'update', 'delete'])
  expect(actual).toEqual(['create', 'read', 'update', 'delete'])
})

test('functionCallback - value is not a function', () => {
  const actual = functionCallback('value')()
  expect(actual).toBe('value')
})

test('functionCallback - value  function', () => {
  const actual = functionCallback(value => value)('value')
  expect(actual).toBe('value')
})

test('functionCallback - value is not a function and nullabe', () => {
  const actual = functionCallback('value', null)()
  expect(actual).toBe(null)
})
