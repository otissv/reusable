export function Box(x) {
  return {
    map: f => Box(f(x)),
    fold: f => f(x),
    log: f => {
      console.log(x)
      return Box(x)
    },
  }
}

export function stringToArray(value) {
  return value != null
    ? typeof value === 'string'
      ? value.split(' ')
      : value
    : []
}

export function functionCallback(value, nullable) {
  return props =>
    typeof value === 'function'
      ? value(props)
      : arguments.length > 1
      ? nullable
      : value
}
