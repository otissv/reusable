import { useState } from 'react'
import { useStateDevtools } from '../devtools'

export function useInput(initialValue) {
  const [value, setValue] = useState(initialValue)

  function handleChange(e) {
    setValue(e.target.value)
  }

  const type = typeof initialValue === 'boolean' ? 'checked' : 'value'

  return {
    [type]: value,
    onChange: handleChange,
  }
}
