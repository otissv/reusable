import { useCallback, useReducer, useRef, useEffect } from 'react'
import cuid from 'cuid'
import R from 'ramda'
import { useIsEqualEffect } from '../isEqualEffect'

//TODO: add hide boolean / function

function modelDefaults({ id, isValid, parent, touched } = {}) {
  return {
    dirty: touched || false,
    id: id || cuid(),
    isValid: isValid || true,
    parent: parent && parent.id,
    touched: touched || false,
  }
}

export function useForm({ initialState, onBlur, onChange, onFocus, onSubmit }) {
  const reducer = useCallback(function reducer(state, payload) {
    const parent = payload.parent ? payload.parent.split('-') : []
    return updateState(state, { ...payload, parent })
  })

  const initialStateRef = useRef()

  const mapInitialState = {
    ...modelDefaults(),
    fields: buildModel({
      fields: initialState.fields,
    }),
  }

  const [model, dispatch] = useReducer(reducer, mapInitialState)

  useIsEqualEffect(
    () => {
      dispatch({
        type: 'FORM_REPLACE',
        model: {
          ...initialState,
          fields: buildModel({
            fields: initialState.fields,
          }),
        },
      })
      initialStateRef.current = initialState
    },
    [initialState, initialStateRef.current]
  )

  function handleBlur(e) {
    const {
      id,
      dataset: { parent },
    } = e.target

    const field = getField(result => result)({
      id,
      parent,
      fields: model.fields,
    })

    field.data.attributes && validator({ id, parent, model, dispatch })

    onBlur && onBlur(e, { field, model, dispatch })
  }

  function handleFocus(e) {
    onFocus && onFocus(e, { field, model, dispatch })
  }

  function handleChange(e) {
    const {
      id,
      dataset: { parent },
    } = e.target
    const field = getField(id)

    const data = {
      text: {
        value: e.target.value,
      },
      checkbox: {
        checked: e.target.checked,
      },
      radio: {
        value: e.target.value,
      },
    }[e.target.type] || { value: e.target.value }
    onChange && onChange(e, { field, model, dispatch })

    dispatch({
      type: 'FORM_UPDATE',
      attributes: data,
      id,
      parent,
    })
  }

  function handleSubmit(e) {
    e.preventDefault()
    const isValid = true

    const getValues = fields => {
      return fields.reduce(
        (
          previous,
          { attributes: { checked, name, value }, children, id, parent }
        ) => {
          const currentValue = () =>
            typeof checked !== 'undefined' ? checked : value

          const values = name
            ? {
                ...previous.values,
                [name]: children
                  ? { ...previous.values[name], ...getValues(children) }
                  : currentValue(),
              }
            : previous.values

          const isValid = validator({ id, parent, model, dispatch })
          const errors = [
            ...previous.errors,
            ...(!isValid ? [{ name, id }] : []),
          ]

          return {
            values,
            errors,
          }
        },
        {
          values: {},
          errors: [],
        }
      )
    }

    onSubmit(getValues(model.fields), model)
  }

  return {
    dispatch,
    model,
    handleChange,
    handleBlur,
    handleSubmit,
  }
}

export function buildModel({ fields, parent = {} }) {
  return fields.map(field => {
    const { attributes, id, isValid, name, touched } = field

    const obj = {
      ...field,
      attributes: {
        ...attributes,
        name: name || (attributes && attributes.name) || parent.name,
      },
      ...modelDefaults({ id, isValid, parent, touched }),
    }

    return field.children
      ? {
          ...obj,
          children: buildModel({
            fields: field.children,
            parent: field,
          }),
        }
      : obj
  })
}

const getField = R.curry(function getField(cb, { fields, id, parent = [] }) {
  const parentArray = Array.isArray(parent) ? parent : parent.split('-')
  const parentId =
    parent.length > 0 && parentArray[0].trim() !== '' && parentArray[0]
  const field = fields.filter(item => item.id === (parentId || id))[0] || {}
  const index = fields.findIndex(item => item.id === (parentId || id))

  return field.children
    ? getField(cb, {
        fields: field.children,
        parent: parentArray.splice(1, parent.length),
        id,
      })
    : cb({ data: field, index })
})

function updateState(
  { fields, ...state },
  { type, attributes, id, model, parent, ...dataRest }
) {
  switch (type) {
    case 'FORM_REPLACE':
      return model
    case 'FORM_UPDATE': {
      const parentId = parent.length > 0 && parent[0].trim() !== '' && parent[0]
      const field = fields.filter(item => item.id === (parentId || id))[0]
      const index = fields.findIndex(item => item.id === (parentId || id))

      const parentData = () => {
        const children = field.children
          ? parent
            ? updateState(
                { fields: field.children },
                {
                  type: 'FORM_UPDATE',
                  ...dataRest,
                  attributes,
                  id,
                  parent: parent.splice(1, parent.length),
                }
              )
            : field.children
          : {}

        return {
          ...field,
          ...(children && { children }),
          dirty: true,
          touched: true,
        }
      }

      const fieldData = () => {
        return {
          ...field,
          ...dataRest,
          attributes: {
            ...field.attributes,
            ...attributes,
          },
          dirty: true,
          touched: true,
        }
      }

      const data = parent.length > 0 ? parentData() : fieldData()

      const isUndefined = value => typeof value === 'undefined'

      return {
        ...state,
        dirty: isUndefined(dataRest.dirty) ? state.dirty : true,
        touched: isUndefined(dataRest.touched) ? state.touched : true,
        isValid: isUndefined(dataRest.isValid)
          ? state.isValid
          : dataRest.isValid,
        fields:
          index === 0
            ? [data, ...fields.slice(1, fields.length)]
            : index === state.length - 1
            ? [...fields.slice(0, fields.length - 1), data]
            : [
                ...fields.slice(0, index),
                data,
                ...fields.slice(index + 1, fields.length),
              ],
      }
    }
    default:
      return state
  }
}

export function validator({ dispatch, id, model, parent }) {
  const field = getField(({ data }) => data)({
    id,
    parent,
    fields: model.fields,
  })

  if (!field) return false
  const { attributes, isValid, validate: fieldValidation } = field

  const { value = '', required, type = 'text', checked } =
    field.attributes || {}

  const payload = { id: id, parent }
  const hasChecked = checked == null
  const hasValue = value || value.trim() !== ''

  const notValid =
    (required && hasChecked && !hasValue) ||
    (fieldValidation && fieldValidation(field, model, dispatch) && hasValue)

  if (!notValid !== isValid) {
    dispatch({
      type: 'FORM_UPDATE',
      ...payload,
      isValid: !notValid,
    })
  }

  return !notValid
}
