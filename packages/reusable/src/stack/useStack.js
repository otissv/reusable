import { useReducer } from 'react'

export function useStack(stackType, initialState = []) {
  const stack = {
    FIFO: {
      add: (state, payload) => [...state, payload],
      remove: (state, payload) =>
        state.length === 0 ? [] : state.slice(1, state.length),
      take: state => state[0],
    },
    LIFO: {
      add: (state, payload) => [...state, payload],
      remove: (state, payload) =>
        state.length === 0 ? [] : state.slice(0, state.length - 1),
      take: state => {
        console.log(state[state.length - 1])
        return state[state.length - 1]
      },
    },
  }[stackType]

  function reducer(state, { type, payload }) {
    switch (type) {
      case 'ADD_TO_STACK':
        return stack.add(state, payload)
      case 'REMOVE_FROM_STACK':
        return stack.remove(state, payload)
      default:
        return state
    }
  }
  const [state, dispatch] = useReducer(reducer, initialState)

  const methods = {
    dispatch,
    remove: payload =>
      dispatch({
        type: 'REMOVE_FROM_STACK',
        payload,
      }),
    add: payload =>
      dispatch({
        type: 'ADD_TO_STACK',
        payload,
      }),
    take: payload => {
      dispatch({
        type: 'REMOVE_FROM_STACK',
        payload,
      })

      return stack.take(state)
    },
  }

  return [state, methods]
}

export function useFIFO(initialState = []) {
  return useStack('FIFO', initialState)
}

export function useLIFO(initialState = []) {
  return useStack('LIFO', initialState)
}
