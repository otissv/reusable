import {
  access,
  canUse,
  cannotUse,
  checkPermissions,
  checkUserHasRoles,
  getRoleAccess,
  matchPermissions,
} from './access'

export const users = [
  { id: 1, name: 'Sam', roles: 'GUEST', permissions: ['update', 'read'] },
  { id: 2, name: 'Otis', roles: 'USER' },
  { id: 3, name: 'Paul', roles: 'MANGER' },
  { id: 4, name: 'Alex', roles: 'ADMIN' },
  { id: 5, name: 'David', roles: 'ADMIN' },
]
export const roles = {
  ADMIN: ['create', 'read', 'update', 'delete'],
  MANGER: ['create', 'read', 'update', 'delete'],
  USER: ['read', 'update'],
  GUEST: ['read'],
}

test('checkUserHasRoles - return empty array if not role', () => {
  const user = { roles: 'GUEST', permissions: ['update', 'read'] }
  const actual = checkUserHasRoles({ user, currentRoles: ['ADMIN'] })

  expect(actual).toEqual([])
})

test('checkUserHasRoles - returns users roles if match', () => {
  const user = { roles: 'GUEST', permissions: ['update', 'read'] }
  const actual = checkUserHasRoles({ user, currentRoles: ['GUEST'] })

  expect(actual).toEqual(['GUEST'])
})

test('getRoleAccess - returns empty array if role does not exist', () => {
  const user = { roles: 'GUEST', permissions: ['update', 'read'] }

  expect(getRoleAccess(['WHO'])(roles)).toEqual([])
})

test('getRoleAccess - returns role permissions if matched', () => {
  const user = { roles: 'GUEST', permissions: ['update', 'read'] }

  expect(getRoleAccess(['ADMIN'])(roles)).toEqual([
    'create',
    'read',
    'update',
    'delete',
  ])
  expect(getRoleAccess(['GUEST'])(roles)).toEqual(['read'])
})

test('matchPermissions - return empty array if not allow is false', () => {
  const active = {
    user: { id: 1, roles: 'GUEST', permissions: ['update', 'read'] },
  }
  const allow = false

  const actual = matchPermissions({
    active,
    bool: true,
    rules: ['update', 'read'],
  })([], allow)

  expect(actual).toEqual([])
})

test('matchPermissions - return empty array if not allow fn result is false', () => {
  const active = {
    user: { id: 1, roles: 'GUEST', permissions: ['update', 'read'] },
  }

  const allow = ({ active }) => false

  const actual = matchPermissions({
    active,
    bool: true,
    rules: ['update', 'read'],
  })([], allow)

  expect(actual).toEqual([])
})

test('matchPermissions - return role', () => {
  const active = {
    user: { id: 1, roles: 'GUEST', permissions: ['update', 'read'] },
  }
  const allow = 'read'

  const actual = matchPermissions({
    active,
    bool: true,
    rules: ['update', 'read'],
  })([], allow)

  expect(actual).toEqual(['read'])
})

test('matchPermissions - allow fn return role', () => {
  const active = {
    user: { id: 1, roles: 'GUEST', permissions: ['update', 'read'] },
  }

  const allow = ({ active }) => 'read'

  const actual = matchPermissions({
    active,
    bool: true,
    rules: ['update', 'read'],
  })([], allow)

  expect(actual).toEqual(['read'])
})

test('matchPermissions - allow fn return role', () => {
  const active = {
    user: { id: 1, roles: 'GUEST', permissions: ['update', 'read'] },
  }

  const allow = ({ active }) => 'delete'

  const actual = matchPermissions({
    active,
    bool: true,
    rules: ['update', 'read'],
  })([], allow)

  expect(actual).toEqual([])
})

test('matchPermissions - return empty array if not allow is false', () => {
  const active = {
    user: { id: 1, roles: 'GUEST', permissions: ['update', 'read'] },
  }
  const allow = 'delete'

  const actual = matchPermissions({
    active,
    bool: true,
    rules: ['update', 'read'],
  })([], allow)

  expect(actual).toEqual([])
})
