import React, { useContext, useState } from 'react'
import { access } from './access'

export const PermissionContext = React.createContext({})

export function PermissionProvider({ children, state, value }) {
  const permissions = useState(value)
  const permissionsState = state || useState(value)

  return (
    <PermissionContext.Provider value={permissionsState}>
      {children}
    </PermissionContext.Provider>
  )
}

// hook
export function usePermissions({ can, cannot, has, roles }) {
  const permissions = useContext(PermissionContext)
  const activeAccess = access({ can, cannot, permissions, has, roles })
  return activeAccess.length > 0
}

// if children, return component else boolean
export function Permission({ can, cannot, children, fallback, has, roles }) {
  const hasAccess = usePermissions({ can, cannot, has, roles })
  return hasAccess ? (children ? children : hasAccess) : fallback
}
