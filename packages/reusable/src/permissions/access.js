import { Box, stringToArray, functionCallback } from '../utils'

export function matchPermissions({ active, bool, rules }) {
  return (previous, allow) => {
    const isAllow = (() => functionCallback(allow, false)({ active, rules }))()

    const filter = value => permission =>
      bool ? permission === value : permission !== value

    const result = isAllow
      ? isAllow === false
        ? []
        : rules.filter(filter(isAllow))
      : rules.filter(filter(allow))

    return [...previous, ...result]
  }
}

export function checkPermissions({ active, bool, list = [], rules }) {
  return list.length > 0
    ? list.reduce(matchPermissions({ active, bool, rules }), [])
    : rules
}

export function checkUserHasRoles({ user, currentRoles }) {
  const userRoles = stringToArray(user.roles)
  const hasRoles = currentRoles.some(role => userRoles.includes(role))
  return hasRoles ? userRoles : []
}

export function getRoleAccess(currentRoles = []) {
  const roles = stringToArray(currentRoles)

  return permissionsRoles => {
    return roles.reduce((previous, role) => {
      return permissionsRoles[role]
        ? [...previous, ...permissionsRoles[role]]
        : previous
    }, [])
  }
}

export function canUse({ can, active, roles }) {
  const list = [...stringToArray(can), ...getRoleAccess(roles)(active.roles)]

  return rules =>
    checkPermissions({
      active,
      bool: true,
      list,
      rules,
    })
}

export function cannotUse({ cannot, active }) {
  return rules =>
    checkPermissions({
      active,
      bool: false,
      list: stringToArray(cannot),
      rules,
    })
}

export function access({ can = [], cannot = [], permissions, has, roles }) {
  const [active] = permissions
  const currentRoles = stringToArray(has)

  return currentRoles.length > 0
    ? checkUserHasRoles({ ...active, currentRoles })
    : Box(active.user.permissions)
        .map(canUse({ can, active, has, roles }))
        .map(cannotUse({ cannot, active }))
        .fold(result => result)
}
