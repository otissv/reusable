import React, {
  lazy,
  useContext,
  useEffect,
  useRef,
  useState,
  Suspense,
} from 'react'
import RouteParser from 'route-parser'
import { useIsEqualEffect } from '../isEqualEffect'
export const RouterContext = React.createContext('')

export const RouterProvider = React.memo(function RouterProvider({
  children,
  value,
  state,
}) {
  const routerState = useState(value)
  const [route, setRoute] = state || routerState

  useEffect(() => {
    window.addEventListener('popstate', e => {
      setRoute(e.currentTarget.location.pathname)
    })
  })

  useEffect(
    () => {
      window.history.replaceState({}, route, route)
    },
    [route]
  )

  return (
    <RouterContext.Provider value={[route, setRoute]}>
      {children}
    </RouterContext.Provider>
  )
})

RouterProvider.defaultProps = {
  value: window.location.pathname,
}

export const Link = React.memo(function Link({
  children,
  onClick,
  onEnter,
  to,
  ...propsRest
}) {
  const [route, setRoute] = useContext(RouterContext)

  function handleClick(e) {
    e.preventDefault()
    if (to !== route) {
      setRoute(to)
      window.history.pushState({}, route, route)
    }

    onClick && onClick(e)
  }

  return (
    <a href={to} onClick={handleClick} {...propsRest}>
      {children}
    </a>
  )
})

export function Redirect({ to }) {
  const router = useContext(RouterContext)
  const setRoute = router[1]

  useEffect(
    () => {
      window.history.pushState({}, to, to)
      setRoute(to)
    },
    [to]
  )

  return null
}

export const Route = React.memo(function Route({ component, onEnter, path }) {
  const [route] = useContext(RouterContext)
  const Component = component
  const pattern = new RouteParser(path)
  const params = pattern.match(route)

  useEffect(
    () => {
      if (params) {
        onEnter && onEnter()
      }
    },
    [route]
  )

  return params ? (
    <Component route={[{ ...route[0], params }, route[1]]} />
  ) : null
})

function flattenRoutes(routes, parent) {
  return routes.reduce((accumulator, route) => {
    return route.children
      ? [...accumulator, ...flattenRoutes(route.children, route)]
      : [
          ...accumulator,
          {
            ...route,
            component:
              (parent &&
                function() {
                  return parent.component.type
                    ? parent.component.type({ routes: parent.children })
                    : parent.component({ routes: parent.children })
                }) ||
              route.component,
          },
        ]
  }, [])
}

export const Screen = React.memo(function RouterComponent({ routes }) {
  const [route] = useContext(RouterContext)

  let params = false
  let Component = null

  for (let item of routes) {
    const pattern = new RouteParser(item.path)
    const match = pattern.match(route)
    if (match) {
      params = match
      Component = item.component
      break
    }
  }

  return params ? (
    <Component route={[{ ...route[0], params }, route[1]]} />
  ) : null
})
export const Router = React.memo(function Router(props) {
  const routes = flattenRoutes(props.routes)

  return <Screen routes={routes} />
})

Router.defaultProps = {
  routes: [],
}
