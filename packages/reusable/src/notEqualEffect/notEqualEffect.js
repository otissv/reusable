import { useEffect } from 'react'
import isEqual from 'lodash/isEqual'
import R from 'ramda'

export const useNotEqualEffect = R.curry(function useNotEqualEffect(
  callback,
  args
) {
  useEffect(() => !isEqual(...args) && callback())
})
