import path from 'path'
import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import replace from 'rollup-plugin-replace'

export default {
  input: './src/index.js',
  moduleName: 'ReactRectanglePopupMenu',
  sourcemap: true,

  targets: [
    {
      dest: './lib/index.js',
      format: 'umd',
    },
    {
      dest: 'lib/index.module.js',
      format: 'es',
    },
  ],

  plugins: [
    babel({
      exclude: 'node_modules/**',
      runtimeHelpers: true,
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
    resolve(),
    commonjs(),
  ],

  external: ['react', 'react-dom'],

  globals: {
    react: 'React',
    'react-dom': 'ReactDOM',
  },
}
